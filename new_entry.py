#!/usr/bin/env python3

# because i'm lazy to type a bunch of text to create files

import os

def get_input(prompt):
	print(prompt)
	return input()

# because these special characters are actually used for something else
def replace_unwanted_chars(text):
	text = text.replace(' ', '_')
	text = text.replace("'", '')
	text = text.replace('"', '')
	text = text.replace('{', '')
	text = text.replace('}', '')
	text = text.replace('[', '')
	text = text.replace(']', '')
	text = text.replace('~', '')
	text = text.replace('`', '')
	text = text.replace('!', '')
	text = text.replace('@', '')
	text = text.replace('#', '')
	text = text.replace('$', '')
	text = text.replace('%', '')
	text = text.replace('^', '')
	text = text.replace('&', "and")
	text = text.replace('*', '')
	return text


name = replace_unwanted_chars(get_input("Name:"))
cred = get_input("Credits:")
cate = get_input("Subdirectory:")

# Join the paths
target = os.path.join(cate, name + ' - ' + cred + '.yaml')

# Get template
with open("template.yaml", "r") as f:
	template = f.read()

# Write the file
with open(target, "w") as f:
	f.write(template)
	print("Wrote to " + target)
