## What?
This repository is just a place to store addon "entries" for [hddons-list](https://gitlab.com/dastrukar/hddons-list) to use that I'm working on.


## Entry file name formatting
```
<ADDON_NAME> - <AUTHOR>
```

The file name doesn't have to exactly follow the name of the addon, but should at least be similar to the name of the addon.

It should be noted that all whitespace should be substituted with `_`.  
Though, you should use whitespace to seperate stuff.  
*(i suck at explaining, so just look at the files for an example)*


## Syntax for entries
The syntax for entries isn't really that hard.  
It's just YAML syntax and only has about 5 (or more?) variables to set.  
Please do note that all variable names are **case sensitive**.

I suggest that you use template.yaml as it's more convinient that way.

Note: It doesn't really matter as to how you order the variables, the only thing that matters is that you include them and set a value to them.

### Example
```YAML
title: "<Title/Name of the addon goes here>"
credits: "by <Author name here>"
flairs:
    - TYPE: "<text>"
flag: "A class to set for this addon's entry goes here. Leave empty if none."
links:
    # You can have more than one link. Note the hyphens
    custom:
        "Custom Link Label": "<Link goes here>"
    git:
        - "Link to your addon's git repository. Leave empty if none"
    forum:
        - "Link to ZDoom Forum about your addon. Leave empty if none"
    direct:
	    - "Link to directly download your addon. Leave empty if none"
description: |
    This uses markdown syntax.
```

### Minimalism
It should be noted that only `title`, `credits`, and `description` are required for the entry to display properly.

Which means you can have an entry that looks like this:
```JSON
title: "Title/Name"
credits: "by Author"
description: |
    This is a description that uses markdown syntax.
```
Though, maybe don't exclude `links`. I mean, how else are people going to download your addon?

### Description syntax
One important thing to note about the description variable is that it uses Markdown syntax.

So the following stuff should work just fine:
`````Markdown
description: |
    **bold**
    *italic*
    [link](url)
    `mono`
    ```
    block of mono
    ```
    
    example(note the spacing):

    - list item 1
    - list item 2
`````

## What's GitLab CI doing?
Every time a commit is pushed, GitLab CI will proceed to get all the JSON files in the given subdirectories (look at .gitlab-ci.yml) and put the contents into a JSON file named `<SUBDIRECTORY>_list.json`.

This list is then used by the [website](https://dastrukar.gitlab.io/hddons-list) to create addon entries.
